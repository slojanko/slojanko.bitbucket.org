var opisi=["<h2>What is acetaminophen?</h2>\
<p>Acetaminophen is a <a href='https://www.drugs.com/drug-class/analgesics.html' onclick='ga('send', 'event', 'Content Link', 'pain reliever');'>pain reliever</a> and a fever reducer.</p>\
<p>Acetaminophen is used to treat many conditions such as headache, muscle aches, <a href='https://www.drugs.com/mcd/arthritis' onclick='ga('send', 'event', 'Content Link', 'arthritis');'>arthritis</a>, backache, toothaches, colds, and <a href='https://www.drugs.com/mcd/fever' onclick='ga('send', 'event', 'Content Link', 'fevers');'>fevers</a>.</p>\
<p>Acetaminophen may also be used for purposes not listed in this medication guide.</p>\
<h2>Important information</h2>\
<p><b>You should not use acetaminophen if you have severe liver disease.</b></p>\
<p>There are many brands and forms of acetaminophen available and not all brands are listed on this leaflet.</p>\
Do not take more of this medication than is recommended. An overdose can damage your liver or cause death. Call your doctor at once if you have nausea, pain in your upper stomach, itching, loss of appetite, dark urine, clay-colored stools, or jaundice (yellowing of your skin or eyes).\
<p>Do not take this medication without a doctor's advice if you have ever had alcoholic liver disease (cirrhosis) or if you drink more than 3 alcoholic beverages per day. You may not be able to take this medicine. Avoid drinking alcohol. It may increase your risk of liver damage while taking acetaminophen.</p>\
<p>Ask a doctor or pharmacist before using any other cold, allergy, pain, or sleep medication. Acetaminophen (sometimes abbreviated as APAP) is contained in many combination medicines. Taking certain products together can cause you to get too much acetaminophen which can lead to a fatal overdose. Check the label to see if a medicine contains acetaminophen or APAP.</p>\
<p>In rare cases, acetaminophen may cause a severe skin reaction. Stop taking this medicine and call your doctor right away if you have skin redness or a rash that spreads and causes blistering and peeling.</p>"
,
"<h2>What is Adderall?</h2>\
<p>Adderall contains a combination of amphetamine and dextroamphetamine. Amphetamine and dextroamphetamine are central nervous system stimulants that affect chemicals in the brain and nerves that contribute to hyperactivity and impulse control.</p>\
<p>Adderall is used to treat <a href='https://www.drugs.com/mcd/narcolepsy' onclick='ga('send', 'event', 'Content Link', 'narcolepsy');'>narcolepsy</a> and <a href='https://www.drugs.com/health-guide/attention-deficit-hyperactivity-disorder-adhd.html' onclick='ga('send', 'event', 'Content Link', 'attention deficit hyperactivity disorder');'>attention deficit hyperactivity disorder</a> (ADHD).</p>\
<p>Adderall may also be used for purposes not listed in this medication guide.</p>\
<h2>Important information</h2>\
<p>You should not use Adderall if you have glaucoma, overactive thyroid, severe agitation, moderate to severe high blood pressure, heart disease or coronary artery disease, or a history of drug or alcohol addiction.</p>\
<p>Do not use Adderall if you have taken an MAO inhibitor in the past 14 days, including isocarboxazid, linezolid, methylene blue injection, phenelzine, rasagiline, selegiline, tranylcypromine, and others.</p>\
<p>Adderall may be habit-forming. <b>Never share this medicine with another person, especially someone with a history of drug abuse or addiction.</b></p>\
<p><b>Using this medicine improperly can cause death or serious side effects on the heart.</b></p>"
,
"<h2>What is alprazolam?</h2>\
<p>Alprazolam is a benzodiazepine (ben-zoe-dye-AZE-eh-peen). It affects chemicals in the brain that may be unbalanced in people with anxiety.</p>\
<p>Alprazolam is used to treat <a href='https://www.drugs.com/mcd/anxiety' onclick='ga('send', 'event', 'Content Link', 'anxiety disorders');'>anxiety disorders</a>, <a href='https://www.drugs.com/mcd/panic-attacks-and-panic-disorder' onclick='ga('send', 'event', 'Content Link', 'panic disorders');'>panic disorders</a>, and anxiety caused by depression.</p>\
<p>Alprazolam may also be used for purposes not listed in this medication guide.</p>\
<h2>Important information</h2>\
<p>You should not use alprazolam if you have narrow-angle glaucoma, if you also take itraconazole or ketoconazole, or if you are allergic to alprazolam or similar medicines (Valium, Ativan, Tranxene, and others).</p>\
<p><b>Do not use alprazolam if you are pregnant.</b> This medicine can cause birth defects or life-threatening withdrawal symptoms in a newborn.</p>\
<p>Alprazolam may be habit-forming. <b>Misuse of habit-forming medicine can cause addiction, overdose, or death.</b></p>\
<p>Do not drink alcohol while taking alprazolam. This medication can increase the effects of alcohol. This medicine may be habit-forming and should be used only by the person for whom it was prescribed. Keep the medication in a secure place where others cannot get to it.</p>"
,
"<h2>What is amitriptyline?</h2>\
<p>Amitriptyline is a tricyclic antidepressant. Amitriptyline affects chemicals in the brain that may be unbalanced in people with depression.</p>\
<p>Amitriptyline is used to treat symptoms of <a href='https://www.drugs.com/mcd/depression-major-depressive-disorder' onclick='ga('send', 'event', 'Content Link', 'depression');'>depression</a>.</p>\
<p>Amitriptyline may also be used for purposes not listed in this medication guide.</p>\
<h2>Important information</h2>\
<p>You should not use this medicine if you have recently had a heart attack.</p>\
<p>Do not use amitriptyline if you have used an MAO inhibitor in the past 14 days, such as isocarboxazid, linezolid, methylene blue injection, phenelzine, rasagiline, selegiline, or tranylcypromine.</p>\
<p>Before taking amitriptyline, tell your doctor if you have used an 'SSRI' antidepressant in the past 5 weeks, such as citalopram (Celexa), escitalopram (Lexapro), fluoxetine (Prozac, Sarafem, Symbyax), fluvoxamine (Luvox), paroxetine (Paxil), or sertraline (Zoloft).</p>\
<p>You may have thoughts about suicide when you first start taking an antidepressant such as amitriptyline, especially if you are younger than 24 years old. Your doctor will need to check you at regular visits for at least the first 12 weeks of treatment.</p>\
<p>Report any new or worsening symptoms to your doctor, such as: mood or behavior changes, anxiety, panic attacks, trouble sleeping, or if you feel impulsive, irritable, agitated, hostile, aggressive, restless, hyperactive (mentally or physically), more depressed, or have thoughts about suicide or hurting yourself.</p>"
,
"<h2>What is amlodipine?</h2>\
<p>Amlodipine is a calcium channel blocker that dilates (widens) blood vessels and improves blood flow.</p>\
<p>Amlodipine is used to treat chest pain <a href='https://www.drugs.com/mcd/angina' onclick='ga('send', 'event', 'Content Link', '(angina)');'>(angina)</a> and other conditions caused by <a href='https://www.drugs.com/mcd/coronary-artery-disease' onclick='ga('send', 'event', 'Content Link', 'coronary artery disease');'>coronary artery disease</a>.</p>\
<p>Amlodipine is also used to treat <a href='https://www.drugs.com/mcd/high-blood-pressure-hypertension' onclick='ga('send', 'event', 'Content Link', 'high blood pressure');'>high blood pressure</a> (hypertension). Lowering blood pressure may lower your risk of a stroke or heart attack.</p>\
<p>Amlodipine is for use in adults and children who are at least 6 years old.</p>\
<h2>Important information</h2>\
<p>Before taking amlodipine, tell your doctor if you have congestive heart failure or liver disease.</p>\
<p>Drinking alcohol can further lower your blood pressure and may increase certain side effects of amlodipine.</p>\
<p>If you are being treated for high blood pressure, keep using amlodipine even if you feel well. High blood pressure often has no symptoms. You may need to use blood pressure medication for the rest of your life.</p>\
<p>Amlodipine is only part of a complete program of treatment that may also include diet, exercise, weight control, and other medications. Follow your diet, medication, and exercise routines very closely.</p>\
<p>Tell your doctor about all other heart or blood pressure medications you are taking.</p>\
<p>Your chest pain may become worse when you first start taking amlodipine or when your dose is increased. Call your doctor if your chest pain is severe or ongoing.</p>"
,
"<h2>What is amoxicillin?</h2>\
<p>Amoxicillin is a penicillin antibiotic that fights bacteria.</p>\
<p>Amoxicillin is used to treat many different types of <a href='https://www.drugs.com/mcd/infectious-diseases' onclick='ga('send', 'event', 'Content Link', 'infection');'>infection</a> caused by bacteria, such as tonsillitis, bronchitis, pneumonia, gonorrhea, and infections of the ear, nose, throat, skin, or urinary tract.</p>\
<p>Amoxicillin is also sometimes used together with another antibiotic called clarithromycin (Biaxin) to treat stomach ulcers caused by Helicobacter pylori infection. This combination is sometimes used with a stomach acid reducer called lansoprazole (Prevacid).</p>\
<p><b>There are many brands and forms of amoxicillin available and not all brands are listed on this leaflet.</b></p>\
<h2>Important information</h2>\
<p>Do not use this medication if you are allergic to amoxicillin or to any other penicillin antibiotic, such as ampicillin (Omnipen, Principen), dicloxacillin (Dycill, Dynapen), oxacillin (Bactocill), penicillin (Beepen-VK, Ledercillin VK, Pen-V, Pen-Vee K, Pfizerpen, V-Cillin K, Veetids), and others.</p>\
<p>Before using amoxicillin, tell your doctor if you are allergic to cephalosporins such as Omnicef, Cefzil, Ceftin, Keflex, and others. Also tell your doctor if you have asthma, liver or kidney disease, a bleeding or blood clotting disorder, mononucleosis (also called 'mono'), or any type of allergy.</p>\
<p>Amoxicillin can make birth control pills less effective. Ask your doctor about using a non-hormone method of birth control (such as a condom, diaphragm, spermicide) to prevent pregnancy while taking this medicine. Take this medication for the full prescribed length of time. Your symptoms may improve before the infection is completely cleared. Amoxicillin will not treat a viral infection such as the common cold or flu. Do not share this medication with another person, even if they have the same symptoms you have.</p>\
<p>Antibiotic medicines can cause diarrhea, which may be a sign of a new infection. If you have diarrhea that is watery or bloody, stop taking amoxicillin and call your doctor. Do not use anti-diarrhea medicine unless your doctor tells you to.</p>"
,
"<h2>What is Ativan?</h2>\
<p>Ativan (lorazepam) belongs to a group of drugs called benzodiazepines. Lorazepam affects chemicals in the brain that may be unbalanced in people with anxiety.</p>\
<p>Ativan is used to treat <a href='//www.drugs.com/mcd/anxiety' onclick='ga('send', 'event', 'Content Link', 'anxiety');'>anxiety</a> disorders.</p>\
<p>Ativan may also be used for purposes not listed in this medication guide.</p>\
<h2>Important information</h2>\
<p>You should not use Ativan if you have narrow-angle glaucoma or myasthenia gravis, or if you are allergic to Valium or a similar medicine.</p>\
<p><b>Do not use lorazepam if you are pregnant.</b> This medicine can cause birth defects or life-threatening withdrawal symptoms in a newborn.</p>\
<p>Lorazepam may be habit-forming and should be used only by the person it was prescribed for. <b>Misuse of habit-forming medicine can cause addiction, overdose, or death.</b> Ativan should never be shared with another person, especially someone who has a history of drug abuse or addiction. Keep the medication in a secure place where others cannot get to it.</p>\
<p>Do not drink alcohol while taking Ativan. Lorazepam can increase the effects of alcohol.</p>"
,
"<h2>What is atorvastatin?</h2>\
<p>Atorvastatin is in a group of drugs called HMG CoA reductase inhibitors, or 'statins.' Atorvastatin reduces levels of 'bad' cholesterol (low-density lipoprotein, or LDL) and triglycerides in the blood, while increasing levels of 'good' cholesterol (high-density lipoprotein, or HDL).</p>\
<p>Atorvastatin is used to treat <a href='https://www.drugs.com/health-guide/high-cholesterol-hypercholesterolemia.html' onclick='ga('send', 'event', 'Content Link', 'high cholesterol');'>high cholesterol</a>, and to lower the risk of stroke, heart attack, or other heart complications in people with type 2 diabetes, coronary heart disease, or other risk factors.</p>\
<p>Atorvastatin is used in adults and children who are at least 10 years old.</p>\
<h2>Important information</h2>\
<p>You should not take atorvastatin if you are pregnant or breast-feeding, or if you have liver disease.</p>\
<p><b>Stop taking this medication and tell your doctor right away if you become pregnant.</b></p>\
<p>Serious drug interactions can occur when certain medicines are used together with atorvastatin. Tell each of your healthcare providers about all medicines you use now, and any medicine you start or stop using.</p>\
<p>In rare cases, atorvastatin can cause a condition that results in the breakdown of skeletal muscle tissue, leading to kidney failure. Call your doctor right away if you have unexplained muscle pain, tenderness, or weakness especially if you also have fever, unusual tiredness, and dark colored urine.</p>\
<p>Avoid eating foods that are high in fat or cholesterol. Atorvastatin will not be as effective in lowering your cholesterol if you do not follow a cholesterol-lowering diet plan.</p>\
<p>Atorvastatin is only part of a complete program of treatment that also includes diet, exercise, and weight control. Follow your diet, medication, and exercise routines very closely.</p>"
,
"<h2>What is azithromycin?</h2>\
<p>Azithromycin is an <a href='https://www.drugs.com/article/antibiotics.html' onclick='ga('send', 'event', 'Content Link', 'antibiotic');'>antibiotic</a> that fights bacteria.</p>\
<p>Azithromycin is used to treat many different types of infections caused by bacteria, such as respiratory infections, skin infections, <a href='https://www.drugs.com/mcd/ear-infection-middle-ear' onclick='ga('send', 'event', 'Content Link', 'ear infections');'>ear infections</a>, and <a href='https://www.drugs.com/mcd/sexually-transmitted-diseases-stds' onclick='ga('send', 'event', 'Content Link', 'sexually transmitted diseases');'>sexually transmitted diseases</a>.</p>\
<p>Azithromycin may also be used for purposes not listed in this medication guide.</p>\
<h2>Important information</h2>\
<p>You should not use azithromycin if you have ever had jaundice or liver problems when you have previously taken this medicine.</p>"
,
"<h2>What is ciprofloxacin?</h2>\
<p>Ciprofloxacin is a fluoroquinolone (flor-o-KWIN-o-lone) antibiotic that fights bacteria in the body. Ciprofloxacin is used to treat different types of bacterial infections. It is also used to treat people who have been exposed to <a href='https://www.drugs.com/mcd/anthrax' onclick='ga('send', 'event', 'Content Link', 'anthrax');'>anthrax</a> or certain types of <a href='https://www.drugs.com/mcd/plague' onclick='ga('send', 'event', 'Content Link', 'plague');'>plague</a>.</p>\
<p>Fluoroquinolone antibiotics can cause serious or disabling side effects. Ciprofloxacin should be used only for infections that cannot be treated with a safer antibiotic.</p>\
<p>Ciprofloxacin may also be used for purposes not listed in this medication guide.</p>\
<h2>Important information</h2>\
<p><b>Ciprofloxacin may cause swelling or tearing of a tendon</b>, especially if you are over 60, if you take steroid medication, or if you have had a kidney, heart, or lung transplant.</p>\
<p>You may not be able to use ciprofloxacin if you have a muscle disorder. Tell your doctor if you have a history of myasthenia gravis.</p>\
<p>You should not use this medication if you are also taking tizanidine.</p>\
<p>Stop taking this medicine and call your doctor at once if you have sudden pain, swelling, bruising, tenderness, stiffness, or movement problems in any of your joints. Rest the joint until you receive medical care or instructions.</p>"
,
"<h2>What is citalopram?</h2>\
<p>Citalopram is an antidepressant in a group of drugs called selective serotonin reuptake inhibitors (SSRIs).</p>\
<p>Citalopram is used to treat <a href='https://www.drugs.com/health-guide/major-depression.html' onclick='ga('send', 'event', 'Content Link', 'depression');'>depression</a>.</p>\
<p>Citalopram may also be used for purposes not listed in this medication guide.</p>\
<h2>Important information</h2>\
<p>You should not use citalopram if you also take pimozide, or if you are being treated with methylene blue injection.</p>\
<p>Do not use citalopram if you have used an MAO inhibitor in the past 14 days, such as isocarboxazid, linezolid, methylene blue injection, phenelzine, rasagiline, selegiline, or tranylcypromine.</p>\
<p>Some young people have thoughts about suicide when first taking an antidepressant. Stay alert to changes in your mood or symptoms. Report any new or worsening symptoms to your doctor.</p>\
<p>Report any new or worsening symptoms to your doctor, such as: mood or behavior changes, anxiety, panic attacks, trouble sleeping, or if you feel impulsive, irritable, agitated, hostile, aggressive, restless, hyperactive (mentally or physically), more depressed, or have thoughts about suicide or hurting yourself.</p>\
<p>Do not give citalopram to anyone younger than 18 years old without the advice of a doctor. This medicine is not approved for use in children.</p>"
,
"<h2>What is clindamycin?</h2>\
<p>Clindamycin is an <a href='https://www.drugs.com/article/antibiotics.html' onclick='ga('send', 'event', 'Content Link', 'antibiotic');'>antibiotic</a> that fights bacteria in the body.</p>\
<p>Clindamycin is used to treat serious <a href='https://www.drugs.com/condition/bacterial-infection.html' onclick='ga('send', 'event', 'Content Link', 'infections');'>infections</a> caused by bacteria.</p>\
<p>Clindamycin may also be used for purposes not listed in this medication guide.</p>\
<h2>Important information</h2>\
<p>Clindamycin can cause diarrhea, which may be severe or lead to serious, life-threatening intestinal problems. <b>If you have diarrhea that is watery or bloody, stop using this medicine and call your doctor.</b></p>\
<p>Before using clindamycin, tell your doctor if you have kidney disease, liver disease, an intestinal disorder such as colitis or Crohn's disease, or a history of asthma, eczema, or allergic skin reaction.</p>\
<p>Take clindamycin for the full prescribed length of time. Your symptoms may improve before the infection is completely cleared. Skipping doses may also increase your risk of further infection that is resistant to antibiotics. Clindamycin will not treat a viral infection such as the common cold or flu.</p>\
<p>If you need surgery, tell the surgeon ahead of time that you are using this medicine. You may need to stop using it for a short time.</p>"
,
"<h2>What is clonazepam?</h2>\
<p>Clonazepam is a benzodiazepine. It affects chemicals in the brain that may be unbalanced. Clonazepam is also a seizure medicine, also called an anti-epileptic drug.</p>\
<p>Clonazepam is used to treat certain <a href='https://www.drugs.com/health-guide/seizure.html' onclick='ga('send', 'event', 'Content Link', 'seizure disorders');'>seizure disorders</a> (including absence seizures or Lennox-Gastaut syndrome) in adults and children.</p>\
<p>Clonazepam is also used to treat <a href='https://www.drugs.com/mcd/panic-attacks-and-panic-disorder' onclick='ga('send', 'event', 'Content Link', 'panic disorder');'>panic disorder</a> (including agoraphobia) in adults.</p>\
<h2>Important information</h2>\
<p>You should not use clonazepam if you have narrow-angle glaucoma or severe liver disease, or if you are allergic to Valium or a similar medicine.</p>\
<p><b>Call your doctor if you have any new or worsening symptoms of depression, unusual changes in behavior, or thoughts about suicide or hurting yourself.</b></p>\
<p>Do not drink alcohol while taking this medicine. Clonazepam may be habit-forming. <b>Never share clonazepam with another person.</b> Keep the medication in a place where others cannot get to it. Selling or giving away clonazepam is against the law.</p>"
,
"<h2>What is codeine?</h2>\
<p>Codeine is an opioid pain medication. An opioid is sometimes called a narcotic.</p>\
<p>Codeine is used to treat mild to moderately severe pain.</p>\
<p>Codeine may also be used for purposes not listed in this medication guide.</p>\
<h2>Important information</h2>\
<p>You should not use codeine if you are allergic to it, or if you have an uncontrolled breathing disorder, a bowel obstruction called paralytic ileus, or frequent asthma attacks or hyperventilation.</p>\
<p>Codeine can slow or stop your breathing, and may be habit-forming. Use only your prescribed dose. Never share codeine with another person.</p>\
<p><b>MISUSE OF NARCOTIC MEDICINE CAN CAUSE ADDICTION, OVERDOSE, OR DEATH, especially in a child or other person using the medicine without a prescription.</b></p>\
<p>Medicines that contain codeine should not be given to a child just after surgery to remove the tonsils or adenoids.</p>\
<p><b>Get emergency medical help if a child taking this medication has</b> breathing problems, blue lips, or severe drowsiness, or if you cannot wake the child up from sleep.</p>"
,
"<h2>What is cyclobenzaprine?</h2>\
<p>Cyclobenzaprine is a <a href='https://www.drugs.com/drug-class/skeletal-muscle-relaxants.html' onclick='ga('send', 'event', 'Content Link', 'muscle relaxant');'>muscle relaxant</a>. It works by blocking nerve impulses (or pain sensations) that are sent to your brain.</p>\
<p>Cyclobenzaprine is used together with rest and physical therapy to treat <a href='https://www.drugs.com/health-guide/muscle-strain.html' onclick='ga('send', 'event', 'Content Link', 'skeletal muscle conditions');'>skeletal muscle conditions</a> such as pain or injury.</p>\
<p>Cyclobenzaprine may also be used for purposes not listed in this medication guide.</p>\
<h2>Important information</h2>\
<p>You should not use cyclobenzaprine if you have a thyroid disorder, heart block, congestive heart failure, a heart rhythm disorder, or you have recently had a heart attack.</p>\
<p>Do not use cyclobenzaprine if you have taken an MAO inhibitor in the past 14 days. A dangerous drug interaction could occur. MAO inhibitors include isocarboxazid, linezolid, phenelzine, rasagiline, selegiline, and tranylcypromine.</p>"
,
"<h2>What is Cymbalta?</h2>\
<p>Cymbalta (duloxetine) is a selective serotonin and norepinephrine reuptake inhibitor antidepressant (SSNRI). The way duloxetine works is still not fully understood. It is thought to positively affect communication between nerve cells in the central nervous system and/or restore chemical balance in the brain.</p>\
<p>Cymbalta is used to treat <a href='https://www.drugs.com/mcd/depression-major-depressive-disorder' onclick='ga('send', 'event', 'Content Link', 'major depressive disorder');'>major depressive disorder</a> in adults. It is also used to treat <a href='https://www.drugs.com/mcd/generalized-anxiety-disorder' onclick='ga('send', 'event', 'Content Link', 'general anxiety disorder');'>general anxiety disorder</a> in adults and children who are at least 7 years old.</p>\
<p>Cymbalta is also used in adults to treat <a href='https://www.drugs.com/mcd/fibromyalgia' onclick='ga('send', 'event', 'Content Link', 'fibromyalgia');'>fibromyalgia</a> (a chronic pain disorder), or chronic muscle or joint pain (such as low back pain and osteoarthritis pain).</p>\
<p>Cymbalta is also used to treat pain caused by nerve damage in adults with diabetes <a href='https://www.drugs.com/mcd/diabetic-neuropathy' onclick='ga('send', 'event', 'Content Link', '(diabetic neuropathy)');'>(diabetic neuropathy)</a>.</p>\
<h2>Important information</h2>\
<p>You should not use Cymbalta if you have untreated or uncontrolled glaucoma, or if you also take thioridazine.</p>\
<p>Do not use Cymbalta if you have used an MAO inhibitor in the past 14 days. A dangerous drug interaction could occur. MAO inhibitors include isocarboxazid, linezolid, methylene blue injection, phenelzine, rasagiline, selegiline, tranylcypromine, and others. After you stop taking Cymbalta, you must wait at least 5 days before you start taking an MAOI.</p>\
<p>Some young people have thoughts about suicide when first taking an antidepressant. Your doctor will need to check your progress at regular visits while you are using Cymbalta. Your family or other caregivers should also be alert to changes in your mood or symptoms.</p>\
<p>Report any new or worsening symptoms to your doctor, such as: mood or behavior changes, anxiety, panic attacks, trouble sleeping, or if you feel impulsive, irritable, agitated, hostile, aggressive, restless, hyperactive (mentally or physically), more depressed, or have thoughts about suicide or hurting yourself.</p>"
,
"<h2>What is doxycycline?</h2>\
<p>Doxycycline is a tetracycline <a href='https://www.drugs.com/article/antibiotics.html' onclick='ga('send', 'event', 'Content Link', 'antibiotic');'>antibiotic</a> that fights bacteria in the body.</p>\
<p>Doxycycline is used to treat many different bacterial infections, such as acne, <a href='https://www.drugs.com/mcd/urinary-tract-infection-uti' onclick='ga('send', 'event', 'Content Link', 'urinary tract infections');'>urinary tract infections</a>, intestinal infections, eye infections, <a href='https://www.drugs.com/mcd/gonorrhea' onclick='ga('send', 'event', 'Content Link', 'gonorrhea');'>gonorrhea</a>, <a href='https://www.drugs.com/mcd/chlamydia' onclick='ga('send', 'event', 'Content Link', 'chlamydia');'>chlamydia</a>, periodontitis (gum disease), and others.</p>\
<p>Doxycycline is also used to treat blemishes, bumps, and acne-like lesions caused by <a href='https://www.drugs.com/mcd/rosacea' onclick='ga('send', 'event', 'Content Link', 'rosacea');'>rosacea</a>. It will not treat facial redness caused by rosacea.</p>\
<p>Some forms of doxycycline are used to prevent malaria, to treat anthrax, or to treat infections caused by mites, ticks, or lice.</p>\
<h2>Important information</h2>\
<p>You should not take doxycycline if you are allergic to any tetracycline antibiotic.</p>\
<p>Children younger than 8 years old should use doxycycline only in cases of severe or life-threatening conditions. This medicine can cause permanent yellowing or graying of the teeth in children.</p>\
<p>Using doxycycline during pregnancy could harm the unborn baby or cause permanent tooth discoloration later in the baby's life.</p>"
,
"<h2>What is gabapentin?</h2>\
<p>Gabapentin is an anti-epileptic medication, also called an anticonvulsant. It affects chemicals and nerves in the body that are involved in the cause of seizures and some types of pain.</p>\
<p>Gabapentin is used in adults to treat <a href='https://www.drugs.com/mcd/postherpetic-neuralgia' onclick='ga('send', 'event', 'Content Link', 'nerve pain');'>nerve pain</a> caused by herpes virus or shingles (herpes zoster).</p>\
<p>The Horizant brand of gabapentin is also used to treat <a href='https://www.drugs.com/mcd/restless-legs-syndrome' onclick='ga('send', 'event', 'Content Link', 'restless legs syndrome');'>restless legs syndrome</a> (RLS).</p>\
<p>The Neurontin brand of gabapentin is also used to treat <a href='https://www.drugs.com/cg/seizures.html' onclick='ga('send', 'event', 'Content Link', 'seizures');'>seizures</a> in adults and children who are at least 3 years old.</p>\
<p><b>Use only the brand and form of gabapentin that your doctor has prescribed.</b> Check your medicine each time you get a refill at the pharmacy, to make sure you have received the correct form of this medication.</p>\
<h2>Important information</h2>\
<p>Follow all directions on your medicine label and package. Tell each of your healthcare providers about all your medical conditions, allergies, and all medicines you use.</p>"
,
"<h2>What is hydrochlorothiazide?</h2>\
<p>Hydrochlorothiazide is a thiazide diuretic (water pill) that helps prevent your body from absorbing too much salt, which can cause fluid retention.</p>\
<p>Hydrochlorothiazide is used to treat <a href='https://www.drugs.com/mcd/high-blood-pressure-hypertension' onclick='ga('send', 'event', 'Content Link', 'high blood pressure');'>high blood pressure</a> (hypertension).</p>\
<p>Hydrochlorothiazide is also used to treat fluid retention <a href='https://www.drugs.com/mcd/edema' onclick='ga('send', 'event', 'Content Link', '(edema)');'>(edema)</a> in people with congestive heart failure, cirrhosis of the liver, or kidney disorders, or edema caused by taking steroids or estrogen.</p>\
<h2>Important information</h2>\
<p>You should not use hydrochlorothiazide if you are unable to urinate.</p>\
<p>Before using hydrochlorothiazide, tell your doctor if you have liver disease, kidney disease, glaucoma, asthma or allergies, gout, diabetes, or if you are allergic to sulfa drugs or penicillin.</p>\
<p>Avoid drinking alcohol, which can increase some of the side effects of this medicine.</p>\
<p>Avoid becoming overheated or dehydrated during exercise and in hot weather. Follow your doctor's instructions about the type and amount of liquids you should drink. In some cases, drinking too much liquid can be as unsafe as not drinking enough.</p>\
<p>There are many other drugs that can interact with hydrochlorothiazide. Tell your doctor about all medications you use. This includes prescription, over-the-counter, vitamin, and herbal products. Do not start a new medication without telling your doctor. Keep a list of all your medicines and show it to any healthcare provider who treats you.</p>\
<p>If you are being treated for high blood pressure, keep using hydrochlorothiazide even if you feel fine. High blood pressure often has no symptoms.</p>"
,
"<h2>What is ibuprofen?</h2>\
<p>Ibuprofen is a nonsteroidal anti-inflammatory drug (NSAID). It works by reducing hormones that cause inflammation and pain in the body.</p>\
<p>Ibuprofen is used to reduce fever and treat pain or inflammation caused by many conditions such as headache, toothache, back pain, <a href='https://www.drugs.com/mcd/arthritis' onclick='ga('send', 'event', 'Content Link', 'arthritis');'>arthritis</a>, <a href='https://www.drugs.com/mcd/menstrual-cramps' onclick='ga('send', 'event', 'Content Link', 'menstrual cramps');'>menstrual cramps</a>, or minor injury.</p>\
<p>Ibuprofen is used in adults and children who are at least 6 months old.</p>\
<h2>Important information</h2>\
<p>Ibuprofen can increase your risk of fatal heart attack or stroke, especially if you use it long term or take high doses, or if you have heart disease. Do not use this medicine just before or after heart bypass surgery (coronary artery bypass graft, or CABG).</p>\
<p>Ibuprofen may also cause stomach or intestinal bleeding, which can be fatal. These conditions can occur without warning while you are using ibuprofen, especially in older adults.</p>\
<p>Do not take more than your recommended dose. <b>An ibuprofen overdose can damage your stomach or intestines.</b> Use only the smallest amount of medication needed to get relief from your pain, swelling, or fever.</p>"
,
"<h2>What is Lexapro?</h2>\
<p>Lexapro (escitalopram) is an antidepressant belonging to a group of drugs called selective serotonin reuptake inhibitors (SSRIs). The way escitalopram works is still not fully understood. It is thought to positively affect communication between nerve cells in the central nervous system and/or restore chemical balance in the brain.</p>\
<p>Lexapro is used to treat <a href='https://www.drugs.com/health-guide/generalized-anxiety-disorder.html' onclick='ga('send', 'event', 'Content Link', 'anxiety');'>anxiety</a> in adults. Lexapro is also used to treat <a href='https://www.drugs.com/health-guide/major-depression.html' onclick='ga('send', 'event', 'Content Link', 'major depressive disorder');'>major depressive disorder</a> in adults and adolescents who are at least 12 years old.</p>\
<p>Lexapro may also be used for purposes not listed in this medication guide.</p>\
<h2>Important information</h2>\
<p>You should not use Lexapro if you also take pimozide, or if you are being treated with methylene blue injection.</p>\
<p>Do not use Lexapro if you have taken an MAO inhibitor in the past 14 days. A dangerous drug interaction could occur. MAO inhibitors include isocarboxazid, linezolid, phenelzine, rasagiline, selegiline, and tranylcypromine.</p>\
<p>Some young people have thoughts about suicide when first taking an antidepressant. Your doctor will need to check your progress at regular visits while you are using Lexapro. Your family or other caregivers should also be alert to changes in your mood or symptoms.</p>\
<p>Do not give this medicine to anyone under 12 years.</p>"
,
"<h2>What is lisinopril?</h2>\
<p>Lisinopril is an ACE inhibitor. ACE stands for angiotensin converting enzyme.</p>\
<p>Lisinopril is used to treat <a href='https://www.drugs.com/mcd/high-blood-pressure-hypertension' onclick='ga('send', 'event', 'Content Link', 'high blood pressure');'>high blood pressure</a> (hypertension) in adults and children who are at least 6 years old.</p>\
<p>Lisinopril is also used to treat congestive heart failure in adults, or to improve survival after a heart attack.</p>\
<h2>Important information</h2>\
<p><b>Do not use lisinopril if you are pregnant.</b> It could harm the unborn baby. <b>Stop using this medicine and tell your doctor right away if you become pregnant.</b></p>not use this medicine if you have hereditary angioedema.</p>\
<p>If you have diabetes, do not use lisinopril together with any medication that contains aliskiren (such as Amturnide, Tekturna, Tekamlo).</p>"
,
"<h2>What is loratadine?</h2>\
<p>Loratadine is an antihistamine that reduces the effects of natural chemical histamine in the body. Histamine can produce symptoms of sneezing, itching, watery eyes, and runny nose.</p>\
<p>Loratadine is used to treat sneezing, runny nose, watery eyes, hives, skin rash, itching, and other cold or <a href='https://www.drugs.com/mcd/allergies' onclick='ga('send', 'event', 'Content Link', 'allergy symptoms');'>allergy symptoms</a>.</p>\
<p>Loratadine is also used to treat skin hives and itching in people with chronic skin reactions.</p>\
<h2>Important information</h2>\
<p>You should not take this medication if you are allergic to loratadine or to desloratadine (Clarinex).</p>\
<p>Follow all directions on your medicine label and package. Tell each of your healthcare providers about all your medical conditions, allergies, and all medicines you use.</p>\
<p>Loratadine disintegrating tablets (Claritin Reditab) may contain phenylalanine. Talk to your doctor before using this form of loratadine if you have phenylketonuria (PKU).</p>\
<p>Ask a doctor or pharmacist before taking this medicine if you have liver or kidney disease.</p>"
,
"<h2>What is lorazepam?</h2>\
<p>Lorazepam belongs to a group of drugs called benzodiazepines. It affects chemicals in the brain that may be unbalanced in people with anxiety.</p>\
<p>Lorazepam is used to treat <a href='//www.drugs.com/mcd/anxiety' onclick='ga('send', 'event', 'Content Link', 'anxiety');'>anxiety</a> disorders.</p>\
<p>Lorazepam may also be used for purposes not listed in this medication guide.</p>\
<h2>Important information</h2>\
<p>You should not use lorazepam if you have narrow-angle glaucoma or myasthenia gravis, or if you are allergic to Valium or a similar medicine.</p>\
<p><b>Do not use lorazepam if you are pregnant.</b> This medicine can cause birth defects or life-threatening withdrawal symptoms in a newborn.</p>\
<p>Lorazepam may be habit-forming and should be used only by the person it was prescribed for. <b>Misuse of habit-forming medicine can cause addiction, overdose, or death.</b> Lorazepam should never be shared with another person, especially someone who has a history of drug abuse or addiction. Keep the medication in a secure place where others cannot get to it.</p>\
<p>Do not drink alcohol while taking lorazepam. This medication can increase the effects of alcohol.</p>"
,
"<h2>What is losartan?</h2>\
<p>Losartan (Cozaar) belongs to a group of drugs called angiotensin II receptor antagonists. It keeps blood vessels from narrowing, which lowers blood pressure and improves blood flow.</p>\
<p>Losartan is used to treat high blood pressure (hypertension). It is also used to lower the risk of stroke in certain people with heart disease.</p>\
<p>Losartan is used to slow long-term kidney damage in people with type 2 diabetes who also have high blood pressure.</p>\
<p>Losartan may also be used for purposes not listed in this medication guide.</p>\
<h2>Important information</h2>\
<p>Do not use if you are pregnant. Stop using and tell your doctor right away if you become pregnant. Losartan can cause injury or death to the unborn baby if you take the medicine during your second or third trimester. Use effective birth control.</p>\
<p>You should not use this medication if you are allergic to losartan.</p>\
<p>If you have diabetes, do not use losartan together with any medication that contains aliskiren (Amturnide, Tekturna, Tekamlo, Valturna).</p>\
<p>In rare cases, losartan can cause a condition that results in the breakdown of skeletal muscle tissue, leading to kidney failure. Call your doctor right away if you have unexplained muscle pain, tenderness, or weakness especially if you also have fever, unusual tiredness, and dark colored urine.</p>"
,
"<h2>What is Lyrica?</h2>\
<p>Lyrica (pregabalin) is an anti-epileptic drug, also called an anticonvulsant. It works by slowing down impulses in the brain that cause seizures. Lyrica also affects chemicals in the brain that send pain signals across the nervous system.</p>\
<p>Lyrica is used to control seizures and to treat fibromyalgia. It is also used to treat pain caused by nerve damage in people with diabetes (diabetic neuropathy), herpes zoster (post-herpetic neuralgia, or neuropathic pain associated with spinal cord injury.</p>\
<p>Lyrica may also be used for other purposes not listed in this medication guide.</p>\
<h2>Important information</h2>\
<p>You may have thoughts about suicide while taking Lyrica. Your doctor will need to check you at regular visits. Do not miss any scheduled appointments.</p>\
<p>Call your doctor at once if you have any new or worsening symptoms such as: mood or behavior changes, depression, anxiety, insomnia, or if you feel agitated, hostile, restless, hyperactive (mentally or physically), or have thoughts about suicide or hurting yourself.</p>\
<p>If you are taking Lyrica to prevent seizures, keep taking the medication even if you feel fine.</p>\
<p>Do not stop using Lyrica without first talking to your doctor, even if you feel fine. You may have increased seizures or withdrawal symptoms such as headache, sleep problems, nausea, and diarrhea. Ask your doctor how to avoid withdrawal symptoms when you stop using Lyrica.</p>\
<p>Do not change your dose of Lyrica without your doctor's advice. Tell your doctor if the medication does not seem to work as well in treating your condition.</p>\
<p>Wear a medical alert tag or carry an ID card stating that you take Lyrica. Any medical care provider who treats you should know that you take seizure medication.</p>"
,
"<h2>What is meloxicam?</h2>\
<p>Meloxicam is a nonsteroidal anti-inflammatory drug (NSAID). It works by reducing hormones that cause inflammation and pain in the body.</p>\
<p>Meloxicam is used to treat pain or inflammation caused by <a href='https://www.drugs.com/mcd/rheumatoid-arthritis' onclick='ga('send', 'event', 'Content Link', 'rheumatoid arthritis');'>rheumatoid arthritis</a> and <a href='https://www.drugs.com/mcd/osteoarthritis' onclick='ga('send', 'event', 'Content Link', 'osteoarthritis');'>osteoarthritis</a> in adults.</p>\
<p>Meloxicam is also used to treat juvenile rheumatoid arthritis in children who are at least 2 years old.</p>\
<h2>Important information</h2>\
<p>Meloxicam can increase your risk of fatal heart attack or stroke, especially if you use it long term or take high doses, or if you have heart disease. Do not use this medicine just before or after heart bypass surgery (coronary artery bypass graft, or CABG).</p>\
<p>Get emergency medical help if you have chest pain, weakness, shortness of breath, slurred speech, or problems with vision or balance.</p>\
<p>Meloxicam may also cause stomach or intestinal bleeding, which can be fatal. These conditions can occur without warning while you are using meloxicam, especially in older adults.</p>\
<p>Call your doctor at once if you have symptoms of stomach bleeding such as black, bloody, or tarry stools, or coughing up blood or vomit that looks like coffee grounds.</p>\
<p>Avoid drinking alcohol. It may increase your risk of stomach bleeding.</p>\
<p>Ask a doctor or pharmacist before using any other cold, allergy, or pain medicine. Medicines similar to meloxicam are contained in many combination medicines. Check the label to see if a medicine contains an NSAID (non-steroidal anti-inflammatory drug) such as aspirin, ibuprofen, ketoprofen, or naproxen.</p>\
<p>Meloxicam can increase your risk of fatal heart attack or stroke, especially if you use it long term or take high doses, or if you have heart disease. Even people without heart disease or risk factors could have a stroke or heart attack while taking this medicine.</p>\
<p>Do not use this medicine just before or after heart bypass surgery (coronary artery bypass graft, or CABG).</p>\
<p>Meloxicam may also cause stomach or intestinal bleeding, which can be fatal. These conditions can occur without warning while you are using meloxicam, especially in older adults.</p>\
<p>You should not use meloxicam if you are allergic to it, or if you have ever had an asthma attack or severe allergic reaction after taking aspirin or an NSAID.</p>\
<p>To make sure meloxicam is safe for you, tell your doctor if you have:</p>\
<ul>\
<li>\
<p>heart disease, high blood pressure, high cholesterol, diabetes, or if you smoke;</p>\
</li>\
<li>\
<p>a history of heart attack, stroke, or blood clot;</p>\
</li>\
<li>\
<p>a history of stomach ulcers or bleeding;</p>\
</li>\
<li>\
<p>asthma;</p>\
</li>\
<li>\
<p>kidney disease (or if you are on dialysis);</p>\
</li>\
<li>\
<p>liver disease; or</p>\
</li>\
<li>\
<p>fluid retention.</p>\
</li>\
</ul>\
<p><b>Taking meloxicam during the last 3 months of pregnancy may harm the unborn baby.</b> Tell your doctor if you are pregnant or plan to become pregnant.</p>\
<p>Meloxicam may cause a delay in ovulation (the release of an egg from an ovary). You should not take this medicine if you are undergoing fertility treatment, or are otherwise trying to get pregnant.</p>\
<p>Meloxicam can pass into breast milk and may harm a nursing baby. You should not breast-feed while using this medicine.</p>\
<p>Meloxicam is not approved for use by anyone younger than 2 years old.</p>"
,
"<h2>What is metformin?</h2>\
<p>Metformin is an oral diabetes medicine that helps control blood sugar levels.</p>\
<p>Metformin is used to improve blood sugar control in people with <a href='https://www.drugs.com/mcd/type-2-diabetes' onclick='ga('send', 'event', 'Content Link', 'type 2 diabetes');'>type 2 diabetes</a>. It is sometimes used in combination with insulin or other medications, but this medicine is not for treating type 1 diabetes.</p>\
<p>Metformin may also be used for purposes not listed in this medication guide.</p>\
<h2>Important information</h2>\
<p>You should not use metformin if you have severe kidney disease, or if you are in a state of diabetic ketoacidosis (call your doctor for treatment with insulin).</p>\
<p>If you need to have any type of x-ray or CT scan using a dye that is injected into your veins, you will need to temporarily stop taking metformin.</p>\
<p>This medicine may cause a serious condition called <b>lactic acidosis.</b> Get emergency medical help if you have even mild symptoms such as: muscle pain or weakness, numb or cold feeling in your arms and legs, trouble breathing, stomach pain, nausea with vomiting, slow or uneven heart rate, dizziness, or feeling very weak or tired.</p>"
,
"<h2>What is metoprolol?</h2>\
<p>Metoprolol is a beta-blocker that affects the heart and circulation (blood flow through arteries and veins).</p>\
<p>Metoprolol is used to treat angina (chest pain) and hypertension (high blood pressure). It is also used to treat or prevent heart attack.</p>\
<p>Metoprolol may also be used for other purposes not listed in this medication guide.</p>\
<h2>Important information</h2>\
<p>You should not use metoprolol if you have a serious heart problem (heart block, sick sinus syndrome, slow heart rate), severe circulation problems, severe heart failure, or a history of slow heart beats that caused fainting.</p>"
,
"<h2>What is naproxen?</h2>\
<p>Naproxen is a <a href='https://www.drugs.com/drug-class/nonsteroidal-anti-inflammatory-agents.html' onclick='ga('send', 'event', 'Content Link', 'nonsteroidal anti-inflammatory drug');'>nonsteroidal anti-inflammatory drug</a> (NSAID). It works by reducing hormones that cause inflammation and pain in the body.</p>\
<p>Naproxen is used to treat pain or inflammation caused by conditions such as <a href='https://www.drugs.com/mcd/arthritis' onclick='ga('send', 'event', 'Content Link', 'arthritis');'>arthritis</a>, <a href='https://www.drugs.com/mcd/ankylosing-spondylitis' onclick='ga('send', 'event', 'Content Link', 'ankylosing spondylitis');'>ankylosing spondylitis</a>, tendinitis, <a href='https://www.drugs.com/mcd/bursitis' onclick='ga('send', 'event', 'Content Link', 'bursitis');'>bursitis</a>, gout, or menstrual cramps.</p>\
<p>The delayed-release or extended-release tablets are slower-acting forms of naproxen that are used only for treating chronic conditions such as arthritis or ankylosing spondylitis. These forms will not work fast enough to treat acute pain.</p>\
<h2>Important information</h2>\
<p>You should not use naproxen if you have a history of allergic reaction to aspirin or other NSAID (nonsteroidal anti-inflammatory drug).</p>\
<p>Naproxen can increase your risk of fatal heart attack or stroke, especially if you use it long term or take high doses, or if you have heart disease. Even people without heart disease or risk factors could have a stroke or heart attack while taking this medicine.</p>\
<p>Do not use this medicine just before or after heart bypass surgery (coronary artery bypass graft, or CABG).</p>\
<p>Get emergency medical help if you have chest pain, weakness, shortness of breath, slurred speech, or problems with vision or balance.</p>\
<p>Naproxen may also cause stomach or intestinal bleeding, which can be fatal. These conditions can occur without warning while you are using this medicine, especially in older adults.</p>"
,
"<h2>What is omeprazole?</h2>\
<p>Omeprazole (Prilosec, Zegerid) belongs to group of drugs called proton pump inhibitors. It decreases the amount of acid produced in the stomach.</p>\
<p>Omeprazole is used to treat symptoms of <a href='https://www.drugs.com/mcd/gerd' onclick='ga('send', 'event', 'Content Link', 'gastroesophageal reflux disease');'>gastroesophageal reflux disease</a> (GERD) and other conditions caused by excess stomach acid. It is also used to promote healing of erosive esophagitis (damage to your esophagus caused by stomach acid).</p>\
<p>Omeprazole may also be given together with antibiotics to treat gastric ulcer caused by infection with helicobacter pylori (H. pylori).</p>\
<p>Omeprazole is not for immediate relief of heartburn symptoms.</p>\
<h2>Important information</h2>\
<p>Omeprazole is not for immediate relief of heartburn symptoms.</p>\
<p>Heartburn is often confused with the first symptoms of a heart attack. Seek emergency medical attention if you have chest pain or heavy feeling, pain spreading to the arm or shoulder, nausea, sweating, and a general ill feeling.</p>\
<p>You should not take this medication if you are allergic to omeprazole or to any other benzimidazole medication such as albendazole or mebendazole.</p>\
<p>Ask a doctor or pharmacist if it is safe for you to take omeprazole if you have liver disease or heart disease, or low levels of magnesium in your blood.</p>\
<p>Some conditions are treated with a combination of omeprazole and antibiotics. Use all medications as directed by your doctor. Read the medication guide or patient instructions provided with each medication. Do not change your doses or medication schedule without your doctor's advice.</p>\
<p>Take omeprazole for the full prescribed length of time. Your symptoms may improve before the infection is completely cleared.</p>\
<p>Prilosec OTC (over-the-counter) should be taken for no longer than 14 days in a row. Allow at least 4 months to pass before you start another 14-day treatment.</p>"
,
"<h2>What is oxycodone?</h2>\
<p>Oxycodone is an opioid pain medication. An opioid is sometimes called a narcotic.</p>\
<p>Oxycodone is used to treat moderate to severe pain.</p>\
<p>Oxycodone extended-release is used for around-the-clock treatment of <a href='https://www.drugs.com/health-guide/pain.html' onclick='ga('send', 'event', 'Content Link', 'pain');'>pain</a>. This form of oxycodone is not for use on an as-needed basis for pain.</p>\
<h2>Important information</h2>\
<p>You should not use oxycodone if you have severe asthma or breathing problems, or a blockage in your stomach or intestines.</p>\
<p>Oxycodone can slow or stop your breathing, especially when you start using this medicine or whenever your dose is changed. <b>Never take this medicine in larger amounts, or for longer than prescribed.</b> Do not crush, break, or open an extended-release pill (Oxycontin). Swallow it whole to avoid exposure to a potentially fatal dose.</p>\
<p>Oxycodone may be habit-forming, even at regular doses. <b>Take this medicine exactly as prescribed by your doctor. Never share the medicine with another person. MISUSE OF NARCOTIC PAIN MEDICATION CAN CAUSE ADDICTION, OVERDOSE, OR DEATH, especially in a child or other person using the medicine without a prescription.</b></p>\
<p>Tell your doctor if you are pregnant. <b>Oxycodone may cause life-threatening withdrawal symptoms in a newborn if the mother has taken this medicine during pregnancy.</b></p>\
<p><b>Do not drink alcohol.</b> Dangerous side effects or death could occur."
,
"<h2>What is pantoprazole?</h2>\
<p>Pantoprazole is a proton pump inhibitor that decreases the amount of acid produced in the stomach.</p>\
<p>Pantoprazole is used to treat erosive <a href='https://www.drugs.com/mcd/esophagitis' onclick='ga('send', 'event', 'Content Link', 'esophagitis');'>esophagitis</a> (damage to the esophagus from stomach acid), and other conditions involving excess stomach acid such as <a href='https://www.drugs.com/mcd/zollinger-ellison-syndrome' onclick='ga('send', 'event', 'Content Link', 'Zollinger-Ellison syndrome');'>Zollinger-Ellison syndrome</a>.</p>\
<p>Pantoprazole is not for immediate relief of heartburn symptoms.</p>\
<h2>Important information</h2>\
<p>You should not use this medication if you are allergic to pantoprazole or to any other benzimidazole medication such as albendazole (Albenza), or mebendazole (Vermox).</p>\
<p>Pantoprazole is not for immediate relief of heartburn symptoms.</p>\
<p>Heartburn is often confused with the first symptoms of a heart attack. Seek emergency medical attention if you have chest pain or heavy feeling, pain spreading to the arm or shoulder, nausea, sweating, and a general ill feeling.</p>\
<p>Take pantoprazole for the full prescribed length of time. Your symptoms may improve before the condition is fully treated.</p>\
<p>Pantoprazole should not be taken together with atazanavir (Reyataz) or nelfinavir (Viracept). Tell your doctor if you are taking either of these medications to treat HIV or AIDS.</p>\
<p>Some conditions must be treated long-term with pantoprazole. Chronic use has caused stomach cancer in animal studies, but it is not known if this medication would have the same effects in humans. Talk with your doctor about your specific risk of developing stomach cancer.</p>\
<p>Long-term treatment with pantoprazole may also make it harder for your body to absorb vitamin B-12, resulting in a deficiency of this vitamin. Talk with your doctor if you need long-term pantoprazole treatment and you have concerns about vitamin B-12 deficiency.</p>"
,
"<h2>What is prednisone?</h2>\
<p>Prednisone is a corticosteroid. It prevents the release of substances in the body that cause inflammation. It also suppresses the immune system.</p>\
<p>Prednisone is used as an anti-inflammatory or an immunosuppressant medication. Prednisone treats many different conditions such as allergic disorders, skin conditions, <a href='https://www.drugs.com/mcd/ulcerative-colitis' onclick='ga('send', 'event', 'Content Link', 'ulcerative colitis');'>ulcerative colitis</a>, <a href='//www.drugs.com/arthritis.html' onclick='ga('send', 'event', 'Content Link', 'arthritis');'>arthritis</a>, lupus, <a href='//www.drugs.com/condition/psoriasis.html' onclick='ga('send', 'event', 'Content Link', 'psoriasis');'>psoriasis</a>, or breathing disorders.</p>\
<h2>Important information</h2>\
<p>Prednisone treats many different conditions such as allergic disorders, skin conditions, ulcerative colitis, arthritis, lupus, psoriasis, or breathing disorders.</p>\
<p>You should not take prednisone if you have a <a href='//www.drugs.com/condition/fungal-infections.html' onclick='ga('send', 'event', 'Content Link', 'fungal infection');'>fungal infection</a> anywhere in your body.</p>\
<p>Steroid medication can weaken your immune system, making it easier for you to get an infection. Avoid being near people who are sick or have infections. Do not receive a 'live' vaccine while using prednisone.</p>\
<p>Call your doctor at once if you have shortness of breath, severe pain in your upper stomach, bloody or tarry stools, severe depression, changes in personality or behavior, vision problems, or eye pain.</p>\
<p>You should not stop using prednisone suddenly. Follow your doctor's instructions about tapering your dose.</p>"
,
"<h2>What is tramadol?</h2>\
<p>Tramadol is a narcotic-like pain reliever.</p>\
<p>Tramadol is used to treat moderate to <a href='https://www.drugs.com/health-guide/pain.html' onclick='ga('send', 'event', 'Content Link', 'severe pain');'>severe pain</a>.</p>\
<p>The extended-release form of tramadol is for around-the-clock treatment of pain. This form of tramadol is not for use on an as-needed basis for pain.</p>\
<p>Tramadol may also be used for purposes not listed in this medication guide.</p>\
<h2>Important information</h2>\
<p>You should not take tramadol if you have used alcohol, sedatives, tranquilizers, or narcotic medications within the past few hours.</p>\
<p>Tramadol can slow or stop your breathing, especially when you start using this medicine or whenever your dose is changed. Never take tramadol in larger amounts, or for longer than prescribed. Do not crush, break, or open an extended-release pill. Swallow it whole to avoid exposure to a potentially fatal dose.</p>\
<p>Seizures (convulsions) have occurred in some people taking this medicine. Tramadol may be more likely to cause a seizure if you have a history of seizures or head injury, a metabolic disorder, or if you are taking certain medicines such as antidepressants, muscle relaxers, narcotic, or medicine for nausea and vomiting</p>\
<p>Tramadol may be habit-forming, even at regular doses. <b>Take this medicine exactly as prescribed by your doctor. Never share the medicine with another person. MISUSE OF NARCOTIC PAIN MEDICATION CAN CAUSE ADDICTION, OVERDOSE, OR DEATH, especially in a child or other person using the medicine without a prescription.</b></p>\
<p><b>Taking tramadol during pregnancy may cause life-threatening withdrawal symptoms in the newborn.</b></p>\
<p><b>Fatal side effects can occur if you use tramadol with alcohol, or with other drugs that cause drowsiness or slow your breathing.</b></p>\
<p><b>Tramadol can interact with many other drugs and cause dangerous side effects or death.</b> Tell your doctor about all your current medicines and any you start or stop using.</p>\
<p>Do not crush the tramadol tablet. This medicine is for oral (by mouth) use only. Powder from a crushed tablet should not be inhaled or diluted with liquid and injected into the body. Using this medicine by inhalation or injection can cause life-threatening side effects, overdose, or death.</p>"
,
"<h2>What is trazodone?</h2>\
<p>Trazodone is an antidepressant medicine. It affects chemicals in the brain that may be unbalanced in people with depression.</p>\
<p>Trazodone is used to treat <a href='https://www.drugs.com/mcd/depression-major-depressive-disorder' onclick='ga('send', 'event', 'Content Link', 'major depressive disorder');'>major depressive disorder</a>.</p>\
<p>Trazodone may also be used for purposes not listed in this medication guide.</p>\
<h2>Important information</h2>\
<p>You should not use trazodone if you are allergic to it, or if you are being treated with methylene blue injection.</p>\
<p>Do not use trazodone if you have taken an MAO inhibitor in the past 14 days. A dangerous drug interaction could occur. MAO inhibitors include isocarboxazid, linezolid, phenelzine, rasagiline, selegiline, and tranylcypromine.</p>\
<p>Some young people have thoughts about suicide when first taking an antidepressant. Your doctor will need to check your progress at regular visits while you are using trazodone. Your family or other caregivers should also be alert to changes in your mood or symptoms.</p>\
<p>Report any new or worsening symptoms to your doctor, such as: mood or behavior changes, anxiety, panic attacks, trouble sleeping, or if you feel impulsive, irritable, agitated, hostile, aggressive, restless, hyperactive (mentally or physically), more depressed, or have thoughts about suicide or hurting yourself.</p>\
<p>Do not give this medicine to anyone younger than 18 years old without the advice of a doctor. Trazodone is not approved for use in children.</p>"
,
"<h2>What is Viagra?</h2>\
<p>Viagra (sildenafil) relaxes muscles found in the walls of blood vessels and increases blood flow to particular areas of the body.</p>\
<p>Viagra is used to treat <a href='https://www.drugs.com/mcd/erectile-dysfunction' onclick='ga('send', 'event', 'Content Link', 'erectile dysfunction');'>erectile dysfunction</a> (impotence) in men. Another brand of sildenafil is Revatio, which is used to treat pulmonary arterial hypertension and improve exercise capacity in men and women.</p>\
<p>Do not take Viagra while also taking Revatio, unless your doctor tells you to.</p>\
<h2>Important information</h2>\
<p>Some medicines can cause unwanted or dangerous effects when used with Viagra. Tell your doctor about all your current medicines, especially riociguat (Adempas).</p>\
<p>Do not take Viagra if you are also using a nitrate drug for chest pain or heart problems, including nitroglycerin, isosorbide dinitrate, isosorbide mononitrate, and some recreational drugs such as 'poppers'. <b>Taking sildenafil with a nitrate medicine can cause a sudden and serious decrease in blood pressure.</b></p>\
<p>Contact your doctor or seek emergency medical attention if your erection is painful or lasts longer than 4 hours. A prolonged erection (priapism) can damage the penis.</p>\
<p>Stop using Viagra and get emergency medical help if you have sudden vision loss.</p>"
,
"<h2>What is Wellbutrin?</h2>\
<p>Wellbutrin (bupropion) is an antidepressant medication used to treat <a href='https://www.drugs.com/mcd/depression-major-depressive-disorder' onclick='ga('send', 'event', 'Content Link', 'major depressive disorder');'>major depressive disorder</a> and <a href='https://www.drugs.com/mcd/seasonal-affective-disorder-sad' onclick='ga('send', 'event', 'Content Link', 'seasonal affective disorder');'>seasonal affective disorder</a>.</p>\
<p>Wellbutrin may also be used for purposes not listed in this medication guide.</p>\
<h2>Important information</h2>\
<p>You should not take Wellbutrin if you have seizures, an eating disorder, or if you have suddenly stopped using alcohol, seizure medication, or sedatives. <b>If you take Wellbutrin for depression, do not also take Zyban to quit smoking.</b></p>\
<p>Do not use bupropion if you have used an MAO inhibitor in the past 14 days. A dangerous drug interaction could occur. MAO inhibitors include isocarboxazid, linezolid, methylene blue injection, phenelzine, rasagiline, selegiline, or tranylcypromine.</p>\
<p>Wellbutrin may cause seizures, especially in people with certain medical conditions or when using certain drugs. Tell your doctor about all of your medical conditions and the drugs you use.</p>\
<p>Some young people have thoughts about suicide when first taking an antidepressant. Your doctor will need to check your progress at regular visits while you are using this medicine. Your family or other caregivers should also be alert to changes in your mood or symptoms.</p>\
<p>Report any new or worsening symptoms to your doctor, such as: mood or behavior changes, anxiety, panic attacks, trouble sleeping, or if you feel impulsive, irritable, agitated, hostile, aggressive, restless, hyperactive (mentally or physically), more depressed, or have thoughts about suicide or hurting yourself.</p>"
,
"<h2>What is Xanax?</h2>\
<p>Xanax (alprazolam) is a benzodiazepine (ben-zoe-dye-AZE-eh-peen). Alprazolam affects chemicals in the brain that may be unbalanced in people with anxiety.</p>\
<p>Xanax is used to treat <a href='https://www.drugs.com/mcd/anxiety' onclick='ga('send', 'event', 'Content Link', 'anxiety disorders');'>anxiety disorders</a>, <a href='https://www.drugs.com/mcd/panic-attacks-and-panic-disorder' onclick='ga('send', 'event', 'Content Link', 'panic disorders');'>panic disorders</a>, and anxiety caused by depression.</p>\
<p>Xanax may also be used for purposes not listed in this medication guide.</p>\
<h2>Important information</h2>\
<p>You should not use Xanax if you have narrow-angle glaucoma, if you also take itraconazole or ketoconazole, or if you are allergic to Xanax or similar medicines (Valium, Ativan, Tranxene, and others).</p>\
<p><b>Do not use Xanax if you are pregnant.</b> This medicine can cause birth defects or life-threatening withdrawal symptoms in a newborn.</p>\
<p>Alprazolam may be habit-forming. <b>Misuse of habit-forming medicine can cause addiction, overdose, or death.</b></p>\
<p>Do not drink alcohol while taking Xanax. This medication can increase the effects of alcohol. Alprazolam may be habit-forming and should be used only by the person for whom it was prescribed. Keep the medication in a secure place where others cannot get to it.</p>"
,
"<h2>What is Zoloft?</h2>\
<p>Zoloft (sertraline) is an antidepressant in a group of drugs called selective serotonin reuptake inhibitors (SSRIs). The way sertraline works is still not fully understood. It is thought to positively affect communication between nerve cells in the central nervous system and/or restore chemical balance in the brain.</p>\
<p>Zoloft is used to treat depression, <a href='https://www.drugs.com/mcd/obsessive-compulsive-disorder-ocd' onclick='ga('send', 'event', 'Content Link', 'obsessive-compulsive disorder');'>obsessive-compulsive disorder</a>, panic disorder, anxiety disorders, <a href='https://www.drugs.com/mcd/post-traumatic-stress-disorder-ptsd' onclick='ga('send', 'event', 'Content Link', 'post-traumatic stress disorder');'>post-traumatic stress disorder</a> (PTSD), and <a href='https://www.drugs.com/mcd/premenstrual-syndrome-pms' onclick='ga('send', 'event', 'Content Link', 'premenstrual dysphoric disorder');'>premenstrual dysphoric disorder</a> (PMDD).</p>\
<p>Zoloft may also be used for purposes not listed in this medication guide.</p>\
<h2>Important information</h2>\
<p>You should not use Zoloft if you also take pimozide, or if you are being treated with methylene blue injection.</p>\
<p>Do not use Zoloft if you have taken an MAO inhibitor in the past 14 days. A dangerous drug interaction could occur. MAO inhibitors include isocarboxazid, linezolid, phenelzine, rasagiline, selegiline, and tranylcypromine.</p>\
<p>Some young people have thoughts about suicide when first taking an antidepressant. Stay alert to changes in your mood or symptoms. Report any new or worsening symptoms to your doctor.</p>\
<p>Report any new or worsening symptoms to your doctor, such as: mood or behavior changes, anxiety, panic attacks, trouble sleeping, or if you feel impulsive, irritable, agitated, hostile, aggressive, restless, hyperactive (mentally or physically), more depressed, or have thoughts about suicide or hurting yourself.</p>\
<p>Do not give Zoloft to anyone younger than 18 years old without the advice of a doctor. Zoloft is FDA-approved for children with obsessive-compulsive disorder (OCD). It is not approved for treating depression in children.</p>"];