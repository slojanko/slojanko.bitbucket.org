
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
* Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
* generiranju podatkov je potrebno najprej kreirati novega pacienta z
* določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
* shraniti nekaj podatkov o vitalnih znakih.
* @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
* @return ehrId generiranega pacienta
*/
 
// SHRANJENI ID IZ ENEGA GENERIRANJA
var ehrID_1 = "9944ce6e-efce-467d-a289-360f5d3a98bc"; // ID Chucka Norissa
var ehrID_2 = "35ad36d1-12dc-44fd-979b-92edbb9a1801"; // Pujsek Pepi
var ehrID_3 = "288d79a3-3417-493b-9f37-4d3afccd6ce7"; // Janez Novak

// TABELA ZDRAVIL ZA NAKLJUCNO GENERIRANJE
var imena_zdravil = 
[
    "Acetaminophen",
    "Adderall",
    "Alprazolam",
    "Amitriptyline",
    "Amlodipine",
    "Amoxicillin",
    "Ativan",
    "Atorvastatin",
    "Azithromycin",
    "Ciprofloxacin",
    "Citalopram",
    "Clindamycin",
    "Clonazepam",
    "Codeine",
    "Cyclobenzaprine",
    "Cymbalta",
    "Doxycycline",
    "Gabapentin",
    "Hydrochlorothiazide",
    "Ibuprofen",
    "Lexapro",
    "Lisinopril",
    "Loratadine",
    "Lorazepam",
    "Losartan",
    "Lyrica",
    "Meloxicam",
    "Metformin",
    "Metoprolol",
    "Naproxen",
    "Omeprazole",
    "Oxycodone",
    "Pantoprazole",
    "Prednisone",
    "Tramadol",
    "Trazodone",
    "Viagra",
    "Wellbutrin",
    "Xanax",
    "Zoloft"
];
 
 
function generirajPodatke() 
{
    console.log("Zacetek generiranja");
    // TODO: Potrebno implementirati
    
    // ehrID_1 
    var ime_1 = "Chucka";
    var priimek_1 = "Norrisa";
    var rojen_1 = "1972-05-15T14:58";
    var spol_1 = "Zenski";
    var visina_1 = [];
    var teza_1 = [];
    var srcni_utrip_1 = [];
    var zdravila_1 = [];

    // ehrID_2
    var ime_2 = "Pujsek";
    var priimek_2 = "Pepi";
    var rojen_2 = "1999-08-15T04:20";
    var spol_2 = "Moski";
    var visina_2 = [];
    var teza_2 = [];
    var srcni_utrip_2 = [];
    var zdravila_2 = [];
    
    // ehrID_3
    var ime_3 = "Janez";
    var priimek_3 = "Novak";
    var rojen_3 = "1938-10-30T14:58";
    var spol_3 = "Moski";
    var visina_3 = [];
    var teza_3 = [];
    var srcni_utrip_3 = [];
    var zdravila_3 = [];
  
  
    // GENERIRAJ NAKLJUCNE PODATKE
    for(var i = 0; i < 8; i++)
    {
        // Generiranje za prvo osebo
        var datum_1 = "|" + (Math.floor(Math.random()*40)+1980)+"."+(Math.floor(Math.random()*12)+1)+"."+(Math.floor(Math.random()*28)+1);
        var enote_1 = "|" + (Math.floor(Math.random()*5+1));
        visina_1[i] = Math.round(Math.random()*10+160);
        teza_1[i] = Math.round(Math.random()*10+70);
        srcni_utrip_1[i] = Math.round(Math.random()*30+60);
        zdravila_1[i] = Math.floor(Math.random()*40) + datum_1 + enote_1;
        
        // Generiranje za drugo osebo
        var datum_2 = "|" + (Math.floor(Math.random()*17)+2000)+"."+(Math.floor(Math.random()*12)+1)+"."+(Math.floor(Math.random()*28)+1);
        var enote_2 = "|" + (Math.floor(Math.random()*7+1));
        visina_2[i] = Math.round(Math.random()*30+50);
        teza_2[i] = Math.round(Math.random()*50+100);
        srcni_utrip_2[i] = Math.round(Math.random()*50+100);
        zdravila_2[i] = Math.floor(Math.random()*40) + datum_2 + enote_2;
        
        // Generiranje za tretjo osebo
        var datum_3 = "|" + (Math.floor(Math.random()*38)+1980)+"."+(Math.floor(Math.random()*12)+1)+"."+(Math.floor(Math.random()*28)+1);
        var enote_3 = "|" + (Math.floor(Math.random()*10+1));
        visina_3[i] = Math.round(Math.random()*25+170);
        teza_3[i] = Math.round(Math.random()*5+80);
        srcni_utrip_3[i] = Math.round(Math.random()*20+70);
        zdravila_3[i] = Math.floor(Math.random()*40) + datum_2 + enote_3;
    }
    

	console.log("Generiranje koncano");
	
    posljiPodatke(1, ime_1, priimek_1, rojen_1, spol_1, visina_1, teza_1, srcni_utrip_1, zdravila_1);
    posljiPodatke(2, ime_2, priimek_2, rojen_2, spol_2, visina_2, teza_2, srcni_utrip_2, zdravila_2);
    posljiPodatke(3, ime_3, priimek_3, rojen_3, spol_3, visina_3, teza_3, srcni_utrip_3, zdravila_3);

}

function posljiPodatke(i, ime, priimek, rojen, spol, visina, teza, srcni_utrip, zdravila)
{
    var sessionId = getSessionId();
    
    // parameter i pove katero osebo, ki smo jo zgenerirali želimo poslati (logično vse 3)
    $.ajaxSetup(
	{
	    headers: {"Ehr-Session": sessionId}
	});
	$.ajax({
	    url: baseUrl + "/ehr",
	    type: 'POST',
	    success: function (data) 
		{
	        var ehrId = data.ehrId;
	        var partyData = 
			{
	            firstNames: ime,
	            lastNames: priimek,
	            dateOfBirth: rojen,
	            partyAdditionalInfo: [
		            {key: "ehrId", value: ehrId}, {key: "spol", value: spol}
		            ,{key: "visina", value: visina.toString()}, {key: "teza", value: teza.toString()} ,
		            {key: "srcniUtrip", value: srcni_utrip.toString()}, {key: "zdravila", value: zdravila.toString()},
		            {key: "7HRt%-6d198$;y4IMO_,?bZ~p>/Yq3", value:"XtM#3zgW5R,'<r8)?}nv/G!)=$J)SHAD_!@"}
	            ]
	        };
	        // Ključ je del podatkov s katerim preverim če ima vse potrebne podatke ma aplikacijo
	        // Brez ključa se podatki pacienta ne prikažejo, tudi če so veljavni! Zaščita pred nepooblaščenim vnosom?
	        // Možnost, da nekdo ugotovi ključ je ZELO majhna, da pa ugotovi vrednost, ki pripada ključu pa še MANJŠA!
	        // DOLZINA KLJUCA: 30
	        // DOLZINA VREDNOSTI: 35
	        // Četudi vse to poteka na odjemalcu, pravi zdravnik ne bo pogledal izvorne kode za ključ in vrednost
	        
	        $.ajax({
	            url: baseUrl + "/demographics/party",
	            type: 'POST',
	            contentType: 'application/json',
	            data: JSON.stringify(partyData),
	            success: function (party) 
				{
				    switch(i)
				    {
				        case 1: ehrID_1 = ehrId;
				            break;
	                    case 2: ehrID_2 = ehrId;
	                        break;
	                    case 3: ehrID_3 = ehrId;
	                        break;
				    }
				    
	                if (party.action == 'CREATE') 
					{
                        $("#napaka").append(
                    	"<div class='alert alert-success alert-dismissable'> \
                    	    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a> \
                    	   	<strong>Uspeh!</strong> Generiranje podatkov uspešno. <br>Ustvarjen je bil ehrID: " + ehrId +
                    	  "</div>" 
                    	);
	                }
	                

	            },
    			error: function(err) 
    			{
    				$("#napaka").append(
    				"<div class='alert alert-danger alert-dismissable'> \
    				    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a> \
    				   	<strong>Napaka!</strong> Generiranje podatkov neuspešno. \
    				  </div>"
    				);
    				
    			}
	        });
	    }
	});
}

// tip == 0, branje preko imena
// tip == 1, branje preko ehrID
function preberiPodatke(tip)
{
    var sessionId = getSessionId();
    
    var id;
    
    // PRIDOBI PRAVILEN ehrID 
    // tip == 0 pomeni Nalozi po imenih
    // tip == 1 pomeni Nalozi po ehrID
    if (tip == 0)
    {
        if ($("#preberiDropDownIme").val().trim() == "")
        {
            $("#napaka").append(
    		"<div class='alert alert-danger alert-dismissable'> \
    		    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a> \
    		   	<strong>Napaka!</strong> Nobena oseba izbrana. \
    		  </div>"
    		);
    		return;
        }
        
        if ($("#preberiDropDownIme").val() == "ehrID_1")
            id = ehrID_1;
        if ($("#preberiDropDownIme").val() == "ehrID_2")
            id = ehrID_2;
        if ($("#preberiDropDownIme").val() == "ehrID_3")
            id = ehrID_3;
        
        $("#preberiEHR").val(id);
    }
    else
    {
        if ($("#preberiEHR").val().trim() == "")
        {
            $("#napaka").append(
    		"<div class='alert alert-danger alert-dismissable'> \
    		    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a> \
    		   	<strong>Napaka!</strong> Nobena oseba izbrana. \
    		  </div>"
    		);
    		return;
        }
        
        id = $("#preberiEHR").val();
    }
    
    $.ajax(
	{
		url: baseUrl + "/demographics/ehr/" + id + "/party",
		type: 'GET',
		headers: {"Ehr-Session": sessionId},
    	success: function (data) 
		{
		    // Shrani podatke v bolj prirocno spremenljivko
		    var party = data.party;
		    
            var kljuc = false;
            // POGLEJ ALI OBSTAJA KLJUC IN VREDNOST
            if (party.partyAdditionalInfo.length == 7)
            {
                for(var i = 0; i < 7; i++)
                {
                    if (party.partyAdditionalInfo[i].key == "7HRt%-6d198$;y4IMO_,?bZ~p>/Yq3")
                    {
                        if (party.partyAdditionalInfo[i].value == "XtM#3zgW5R,'<r8)?}nv/G!)=$J)SHAD_!@")
                        {
                            kljuc = true;
                            break;
                        }        
                    }
                }
            }
            
            if (kljuc == false)
            {
                $("#napaka").append(
        			   "<div class='alert alert-danger alert-dismissable'> \
        			    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a> \
        			   	<strong>Napaka!</strong> Izbrana oseba nima dovolj podatkov oz. ni bila generirana s to aplikacijo. \
        			  </div>"
    			);
    			console.log("Ta oseba nima dovolj podatkov oz. ni generirana s to aplikacijo")
                return;
            }
            
            console.log("Oseba ima dovolj podatkov")
            
            // Osnovni podatki (brez spola) so zapisani izven additionalInfo
			$("#izpisiIme").html(party.firstNames);
			$("#izpisiPriimek").html(party.lastNames);
			$("#izpisiDatumRojstva").html(party.dateOfBirth);
            
			// Pojdi skozi vse zapisane podatke in izlusci potrebne
			for(var i = 0; i < 7; i++)
			{
			    if (party.partyAdditionalInfo[i].key == "spol")
			        $("#izpisiSpol").html(party.partyAdditionalInfo[i].value);
			
			    if (party.partyAdditionalInfo[i].key == "visina")
			    {
			        var visine = JSON.parse("["+party.partyAdditionalInfo[i].value+"]");
			        $("#izpisiVisino").html(visine[visine.length-1]+" cm");
			    }
			    
			    if (party.partyAdditionalInfo[i].key == "teza")
			    {
			        var teze = party.partyAdditionalInfo[i].value.split(",");
			        $("#izpisiTezo").html(teze[teze.length-1]+" kg");
			    }
			    
			    if (party.partyAdditionalInfo[i].key == "srcniUtrip")
			    {
			        var srcniUtripi = party.partyAdditionalInfo[i].value.split(",");
			        $("#izpisiSrcniUtrip").html(srcniUtripi[srcniUtripi.length-1]+" bpm");
			        ustvariGraf(srcniUtripi);
			    }
			    
			    if (party.partyAdditionalInfo[i].key == "zdravila")
			    {
			        var zdravila = party.partyAdditionalInfo[i].value.split(",");
			        $("#tabela_receptov").html("");
			        
			        var izpis = "Zdravila: ";
			        for(var j = 0; j < zdravila.length; j++)
			        {
			            var posamezno_zdravilo = zdravila[j].split("|");
			            izpis += imena_zdravil[parseInt(posamezno_zdravilo)] + " | ";
    			        $("#tabela_receptov").append(
            			"<tr>\
                	        <td>" + imena_zdravil[parseInt(posamezno_zdravilo[0])] + "</td>\
                	        <td>" + posamezno_zdravilo[1] + "</td>\
                	        <td>" + posamezno_zdravilo[2] + " enot</td>\
            	        </tr>"
    			        );
			        }
			        console.log(izpis);
			    }
			}
		},
		error: function(err) 
		{
			$("#napaka").append(
			"<div class='alert alert-danger alert-dismissable'> \
			    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a> \
			   	<strong>Napaka!</strong> Oseba s tem imenom ali ehrID ne obstaja. \
			  </div>"
			);
		}
	});
}

// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
function ustvariGraf(podatki)
{
    
    $("#zbrisiGraf").html("<canvas id='myChart' width=200 height=195></canvas>");
    
    
    var ctx = document.getElementById('myChart').getContext('2d');
    ctx.canvas.width = 200;
    ctx.canvas.height = 200;
    
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ["Jan/15", "Mar/15", "Maj/15", "Jul/15", "Okt/15", "Jan/16", "Mar/16", "Maj/16"],
            datasets: [{
                label: 'bpm',
                data: podatki,
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });

}

function prikaziZdravilo(j)
{
    // Vstavi kratek opis zdravila in povezavo do vira
    $("#opis_zdravil").html(
        opisi[parseInt(j)] +("<p><a href='https://www.drugs.com/" + imena_zdravil[j] + ".html'>Vir (dodatne informacije)</a></p")
        
    );
    console.log("Prikazano zdravilo " + imena_zdravil[j] + " (" + j + ")");
}

$(document).ready(function() {
    
    // IZPOLNI TABELO PREGLED NAD ZDRAVILI
    $("#tabela_zdravil").html("");
    for(var j = 0; j < imena_zdravil.length; j++)
    {
        $("#tabela_zdravil").append(
    	"<tr>\
            <td><button type='button' class='btn btn-primary btn-block' onclick='prikaziZdravilo(" + j + ")'>" + imena_zdravil[j] + "</button> </td>\
        </tr>"
        );
    }
    
    console.log("Stran nalozena");
    prikaziZdravilo(0);
})